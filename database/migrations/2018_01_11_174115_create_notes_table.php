<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('message');
            $table->integer('access_level_id')->unsigned();
            $table->foreign('access_level_id')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->string('member_id');
            $table->foreign('member_id')->references('code')->on('members')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
        Schema::dropIfExists('notes', function (Blueprint $table) {
            $table->dropForeign('note_access_level_id');
            $table->dropForeign('note_member_id');
        });

    }
}
