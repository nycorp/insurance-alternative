<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_fields', function (Blueprint $table) {
            $table->increments('code');
            $table->integer('firstName')->unsigned();
            $table->foreign('firstName')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->integer('lastName')->unsigned();
            $table->foreign('lastName')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->integer('cni')->unsigned();
            $table->foreign('cni')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->integer('email')->unsigned();
            $table->foreign('email')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->integer('phone')->unsigned();
            $table->foreign('phone')->references('id')->on('access_levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_fields');

    }
}
