<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $this->call(MemberTableSeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(CompanyTableSeeder::class);

        $this->call(Access_LevelTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(Member_FieldsTableSeeder::class);

    }
}
