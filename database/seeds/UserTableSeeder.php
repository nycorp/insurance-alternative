<?php

use App\Company;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();
        DB::table('users')->insert([
            'lastName' => 'admin',
            'firstName' => 'admin',
            'phone' => '677009299',
            'email' => 'leticia.kegna@letsrealize.com',
            'role_id' => Role::where('name', 'super administrator')->first()->id,
            'company_id' => Company::where('name', 'lets develop')->first()->id,
            'password' => bcrypt('letsrealizes'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('users')->insert([
            'lastName' => 'admin',
            'firstName' => 'admin',
            'phone' => '677009299',
            'role_id' => Role::where('name', 'super administrator')->first()->id,
            'company_id' => Company::where('name', 'lets develop')->first()->id,
            'email' => 'yann39@live.com',
            'password' => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
