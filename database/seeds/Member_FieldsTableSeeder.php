<?php

use App\Access_Level;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class Member_FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('member_fields')->delete();
        DB::table('member_fields')->insert([
            'code' => Access_Level::where('name', 'public')->first()->id,
            'firstName' => Access_Level::where('name','public')->first()->id,
            'lastName' => Access_Level::where('name','public')->first()->id,
            'cni' => Access_Level::where('name','public')->first()->id,
            'email' => Access_Level::where('name','private')->first()->id,
            'phone' => Access_Level::where('name','private')->first()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
