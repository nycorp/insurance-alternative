<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class Access_LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access_levels')->delete();

        DB::table('access_levels')->insert([
            'name' => 'public',
            'role_id' => Role::where('name','viewer')->first()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('access_levels')->insert([
            'name' => 'private',
            'role_id' => Role::where('name','user')->first()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
