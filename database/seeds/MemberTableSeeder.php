<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('members')->delete();

        for ($i = 0; $i < 10; $i++) {
            DB::table('members')->insert([
                'code' => str_random(20),
                'lastName' => 'lastName' . $i,
                'firstName' => 'firstName',
                'phone' => '67700929' . $i,
                'cni' => str_random(10),
                'image' => 'admin',
                'email' => 'member' . $i . '@gmail.com',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);
        }
    }
}
