<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();
        DB::table('companies')->insert([
            'name' => 'lets develop',
            'member_data' => 'member',
            'member_fields' => 'member',
            'member_note' => 'member',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
