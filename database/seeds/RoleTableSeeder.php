<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->delete();
        DB::table('roles')->insert([
            'name' => 'viewer',
            'level' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('roles')->insert([
            'name' => 'user',
            'level' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('roles')->insert([
            'name' => 'super user',
            'level' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('roles')->insert([
            'name' => 'administrator',
            'level' => 4,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('roles')->insert([
            'name' => 'super administrator',
            'level' => 5,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
