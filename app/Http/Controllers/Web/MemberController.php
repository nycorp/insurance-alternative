<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Mobile\User\MemberController as MemberFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use UxWeb\SweetAlert\SweetAlert;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('member_profile');
    }

    public function find(Request $request)
    {
        $member = MemberFunction::getMember(Auth::user(), $request->input('code'));

        $found = true;
        if (empty($member)) {
            alert()->error('Nous n\'avons pas trouvé cette personne', 'Désolé!')->autoclose(3000);
            return redirect()->back();
        }
        return View::make('member_profile', ['found' => $found,
            'member' => $member]);
    }

    public function getAllMember()
    {
       //$image = file_get_contents('le chemin du fichier selectionner');
        //file_put_contents('/images/image.jpg', $image); //Where to save the image on your server

        $members = DB::table("members")->paginate(5);
        $final_member = array();
        foreach ($members as $member) {
            $member_data_filter = array();
            $member = MemberFunction::filterKey($member);
            foreach ($member as $item => $value) {
                if (strcmp($item, 'created_at') != 0 and strcmp($item, 'updated_at') != 0) {
                    $member_data_filter[MemberFunction::trim_value($item)]
                        = $value;
                }
            }
            array_push($final_member, $member_data_filter);
        }
        return View::make('member_list', ['members' => $final_member, 'unfilter' => $members]);
    }

    public function updateMembersList(Request $request){
        dd($request->get('picture'));
    }

}
