<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Mail\SendVerificationCode;
use App\Mail\SendVerificationContact;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Mobile\User\MemberController as MemberFunction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAddUserForm()
    {
        $company = Company::all();
        $role = Role::all()->where('name', '!=', 'super administrator');
        return View::make('ajout_user', ['roles' => $role, 'companies' => $company]);
    }

    public function contactUs(Request $request)
    {
        $data['lastname'] = $request->input('lastname');
        $data['firstname'] = $request->input('firstname');
        $data['email'] = $request->input('email');
        $data['subject'] = $request->input('subject');
        $data['message'] = $request->input('message');

        $this->validate($request, [

            'firstname' => 'bail|required',
            'lastname' => 'bail|required',
            'subject' => 'bail|required',
            'email' => 'bail|required',
            'message' => 'bail|required'
        ]);

        Mail::to(env('MAIL_USERNAME'))->send(new SendVerificationContact($data,
            route('home')));

        return \view('contacts');
    }

    public function ajout(Request $request)
    {
        $users = new User;

        $users->lastName = $request->input('lastName');
        $users->firstName = $request->input('firstName');
        $users->email = $request->input('email');
        $users->phone = $request->input('phone');
        $users->password = bcrypt(Input::get("password"));
        $users->role_id = $request->input('role_id');
        $users->company_id = $request->input('company_id');
        $users->save();


        return view('ajout_user');
    }

    public function getAllUser()
    {
        $user = User::all()->where('role_id', '<', Role::where('name', 'super administrator')->first()->level);
        return view('user_list')->with('user', MemberFunction::filterKey($user));
    }

    public function edit()
    {
        $user = User::find(Auth::id());
        $role = Role::all();
        $companies = Company::all();

        return view('profile')->with(['user' => $user, 'role' => $role, 'companies' => $companies]);

    }


    public function modifier(Request $request)
    {
        $this->validate($request, [

            'firstName' => 'bail|required',
            'lastName' => 'bail|required',
            'phone' => 'bail|required',
            'email' => 'bail|required',
            'role_id' => 'bail|required',
            'company_id' => 'bail|required',
        ]);

        User::where('id', Auth::id())->update(array(
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'role_id' => $request->input('role_id'),
            'company_id' => $request->input('company_id'),
        ));

        return redirect()->route('edit');
    }

    public function change()
    {
        return view('profile');
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'old' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::find(Auth::id());
        $hashedPassword = $user->password;

        if (Hash::check($request->old, $hashedPassword)) {
            //Change the password
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();

            $request->session()->flash('success', 'Your password has been changed.');

            return back();
        }

        $request->session()->flash('failure', 'Your password has not been changed.');

        return back();


    }
}
