<?php

namespace App\Http\Controllers\Auth;

use App\Mail\SendVerificationCode;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lastname' => 'required|string|max:255',
            'firstname' => 'required|string|max:255',
            'phone' => 'required|string|max:12|unique:users',
            'role' => 'required|numeric',
            'company' => 'required|numeric',
            'email' => 'required|string|email|max:255|unique:users'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $password = str_random(8);
        Mail::to($data['email'])->send(new SendVerificationCode(['username' => $data['email'],
            'password' => $password], route('home')));

        return User::create([
            'email' => $data['email'],
            'lastName' => $data['lastname'],
            'firstName' => $data['firstname'],
            'phone' => $data['phone'],
            'role_id' => $data['role'],
            'company_id' => $data['company'],
            'password' => bcrypt($password),
        ]);
    }
}
