<?php

namespace App\Http\Controllers\Mobile\User;

use App\Http\Controllers\Controller;
use App\Mobile\AccessLevel;
use App\Mobile\Member;
use App\Mobile\MemberField;
use App\Mobile\Note;
use App\Mobile\Role;
use App\Mobile\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class MemberController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        Config::set('jwt.user', 'App\Mobile\User');
        Config::set('auth.providers.users.model', User::class);
        $this->user = $user;
    }

    public function find(Request $request)
    {
        $data = json_decode($request->getContent());
        $member_code = $data->member->code;

        if (empty($member_code)) {
            return response()->json([
                'status' => false,
                'message' => 1400,
            ]);
        }

        //try to retrieve the owner of the car
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                //user not found
                return response()->json([
                    'status' => false,
                    'message' => 4,
                ]);
            }

        } catch (TokenExpiredException $e) {
            //refresh the Token and send it back to user
            $token = JWTAuth::refresh(JWTAuth::getToken());
            //Token expired
            return response()->json([
                'status' => false,
                'message' => 1,
                'token' => $token
            ]);
        } catch (TokenInvalidException $e) {
            //Invalid Token
            return response()->json([
                'status' => false,
                'message' => 2,
            ]);
        } catch (JWTException $e) {
            //Token absent
            return response()->json([
                'status' => false,
                'message' => 3,
            ]);
        }

        $member = $this->getMember($user, $member_code);
        if (empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 1401,
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 1402,
            'member' => $member
        ]);

    }

    /**
     * @param $user
     * @param $code
     * @return mixed|null
     */
    public static function getMember($user, $code)
    {
        //select all fields
        $field = MemberField::all()->first();
        $level_data = array();
        $field = json_decode(json_encode($field));
        $user_level = Role::where('id', $user->role_id)->first()->level;
        //define grant level
        foreach ($field as $key => $value) {
            $access_level_role_id = AccessLevel::where('id', $value)->first()->role_id;
            $level = Role::where('id', $access_level_role_id)->first()->level;
            if ($user_level >= $level) {
                array_push($level_data, $key);
            }
        }
        //Build element to choose
        if (!empty($level_data)) {
            //getting information
            $member = Member::query()->where('code', $code)->get($level_data)->first();
            $final_data = array();
            if (empty($member)) {
                return null;
            }
            $member = self::filterKey($member);
            foreach ($member as $key => $value) {
                $final_data[self::trim_value($key)] = $value;
            }
            return $final_data;
        }

        return null;
    }

    /**
     * @param $value
     * @return string
     */
    public static function trim_value(&$value)
    {
        return ucfirst(str_replace("_", ' ', trim($value, " ")));
    }

    public static function filterKey($member)
    {
        return json_decode(json_encode($member));
    }

    public function postNote(Request $request)
    {
        $data = json_decode($request->getContent());
        $note['title'] = $data->note->title;
        $note['message'] = $data->note->message;
        $note['access'] = $data->note->accessID;
        $note['member'] = $data->note->memberID;

        //try to retrieve the owner of the car
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                //user not found
                return response()->json([
                    'status' => false,
                    'message' => 4,
                ]);
            }

        } catch (TokenExpiredException $e) {
            //refresh the Token and send it back to user
            $token = JWTAuth::refresh(JWTAuth::getToken());
            //Token expired
            return response()->json([
                'status' => false,
                'message' => 1,
                'token' => $token
            ]);
        } catch (TokenInvalidException $e) {
            //Invalid Token
            return response()->json([
                'status' => false,
                'message' => 2,
            ]);
        } catch (JWTException $e) {
            //Token absent
            return response()->json([
                'status' => false,
                'message' => 3,
            ]);
        }

        $validator = Validator::make($note, [
            'title' => 'required',
            'message' => 'required',
            'access' => 'required|numeric',
            'member' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 1502
            ]);
        }

        $note_to_save = new Note();
        $note_to_save->title = $note['title'];
        $note_to_save->message = $note['message'];
        $note_to_save->access_level_id = $note['access'];
        $note_to_save->member_id = $note['member'];
        $success = $note_to_save->save();

        if (!$success) {
            return response()->json([
                'status' => false,
                'message' => 1501
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 1500
        ]);

    }

}
