<?php

namespace App\Http\Controllers\Mobile\Auth\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        Config::set('jwt.user', 'App\Mobile\User');
        Config::set('auth.providers.users.model', \App\Mobile\User::class);
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        //decode receive information
        $data = json_decode($request->getContent());
        $credentials['email'] = $data->user->email;
        $credentials['password'] = $data->user->password;
        $token = null;

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()
                ->json([
                    'status' => false,
                    'message' => 1004,
                ], 422);
        }

        $token = JWTAuth::attempt($credentials);

        if ($token) {
            JWTAuth::setToken($token);
            //check if the account is active
            /*  if (strcmp(JWTAuth::toUser()->status, 'inactive') == 0) {
                  return response()->json(['status' => true,
                      'message' => 1201]);
              }*/

            $user = JWTAuth::toUser();

            //update account status to offline
            // $user->update(array("status" => "online"));

            //prepare image for upload
            //   if (strcmp($user->profile_picture, "default") != 0) {
            //     $profile_pic = Image::make($user->profile_picture)->encode(Image::make($user->profile_picture)->extension, 25);
            //   $user->profile_picture = base64_encode($profile_pic);
            //}

            //return user information
            return response()->json(['status' => true,
                'message' => 1000,
                'token' => $token,
                'user' => $user]);
        } else {
            //check error level
            $user = User::where('email', '=', $credentials['email'])->first();
            if (empty($user) || $user === null) {
                //wrong username
                return response()->json(['status' => false, 'message' => 1001], 401);
            } else {
                //wrong password
                return response()->json(['status' => false, 'message' => 1002], 401);
            }
        }
    }

    /**
     * Get the user by token.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(Request $request)
    {
        JWTAuth::setToken($request->get('token'));
        $user = JWTAuth::toUser();
        return response()->json($user);
    }
}
