<?php

namespace App\Mobile;

use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    protected $table = "access_levels";
}
