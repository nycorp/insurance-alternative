<?php

namespace App\Mobile;

use Illuminate\Database\Eloquent\Model;

class MemberField extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function level()
    {
        return $this->hasOne('App\Mobile\AccessLevel');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
