<?php

namespace App\Mobile;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Get the role record associated with the user.
     */
    public function user()
    {
        return $this->hasOne('App\Mobile\User');
    }

}
