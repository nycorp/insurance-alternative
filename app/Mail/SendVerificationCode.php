<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVerificationCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $credentials;
    protected $verification_link;

    public function __construct($credentials, $link)
    {
        $this->credentials = $credentials;
        $this->verification_link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = "devletsrealize@gmail.com";
        $name = 'Insurance';
        $subject = 'Credentials';

        return $this->view('auth.verification_code')
            ->from($address, $name)
            ->subject($subject)
            ->with(['credentials' => $this->credentials, 'url' => $this->verification_link]);
    }

}