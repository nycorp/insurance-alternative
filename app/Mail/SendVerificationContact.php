<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVerificationContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    protected $verification_link;

    public function __construct($data, $link)
    {
        $this->data = $data;
        $this->verification_link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = "devletsrealize@gmail.com";
        $name = 'Insurance';
        $subject = 'Credentials';

        return $this->view('auth.verification_contact')
            ->from($address, $name)
            ->subject($subject)
            ->with(['data' => $this->data, 'url' => $this->verification_link]);
    }

}