<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'member_data', 'member_fields','member_note',
    ];
}
