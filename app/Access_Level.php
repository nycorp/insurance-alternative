<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access_Level extends Model
{
    protected $table = 'access_levels';
}
