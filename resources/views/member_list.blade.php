@extends('layout.app')

@section('titre')
    Profil membre
@endsection

@section('content')
    <section class="page-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <form action="{{ route('profile') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <!-- Store Search -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="block d-flex">
                                        <input type="text" name="code" class="form-control mb-2 mr-sm-2 mb-sm-0"
                                               id="search"
                                               placeholder="Entrer le code a rechercher" required>
                                        <!-- Search Button -->
                                        <button class="btn btn-success">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard section">
        <!-- Container Start -->
        <div class="container">
            <!-- Row Start -->
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
                    <div class="sidebar">
                        <!-- User Widget -->
                        <div class="widget user-dashboard-profile">
                            <!-- User Image -->
                            <div class="profile-thumb">
                                <img src="{{asset('assets/images/user/user-thumb.png')}}" alt="" class="rounded-circle">
                            </div>
                            <!-- User Name -->
                            <h5 class="text-center">{{ Auth::user()->lastName }}</h5>
                            <p>{{\App\Role::where('id',Auth::user()->role_id)->first()->name}}</p>
                            <a href="{{route('edit')}}" class="btn btn-main-sm">Edit Profile</a>
                        </div>
                        <!-- Dashboard Links -->
                        <div class="widget user-dashboard-menu">
                            <ul>
                                <li class="active"><a href=""><i class="fa fa-user"></i> Nos clients
                                        <span>{{\App\Member::all()->count()}}</span></a></li>
                                <!-- File chooser -->

                                <!--<li><a href=""><i class="fa fa-bookmark-o"></i> Favourite Ads <span>5</span></a></li>
                                <li><a href=""><i class="fa fa-file-archive-o"></i>Archived Ads <span>12</span></a></li>
                                <li><a href=""><i class="fa fa-bolt"></i> Pending Approval<span>23</span></a></li>
                                <li><a href=""><i class="fa fa-cog"></i> Logout</a></li>
                                <li><a href=""><i class="fa fa-power-off"></i>Delete Account</a></li>-->
                            </ul>
                        </div>
                        <div class="widget user-dashboard-profile">

                            <form action="{{route('update/members')}}" method="POST">
                                {{csrf_field()}}
                        <div class="form-group choose-file">
                            <h3 class="widget-header" style="background: #19de86" >Mettre à jour la liste des membres </h3>
                            <input type="file" class="form-control-file d-inline" id="input-file" name="picture" value="">

                        </div>
                                <!-- Submit button -->
                                <button class="nav-link add-button"> <i class="fa fa-plus-circle"></i> Mettre à jour </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                    <!-- Recently Favorited -->
                    <div class="widget dashboard-container my-adslist">
                        <h3 class="widget-header">Nos clients</h3>
                        <table class="table table-responsive product-dashboard-table">
                            <thead>
                            <tr>
                                <th>Profil</th>
                                <th>Information</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td class="product-thumb">
                                        <div class="profile-thumb">
                                            <img src="{{asset('assets/images/user/user-thumb.png')}}"
                                                 alt="Member picture"
                                                 class="rounded-circle">
                                            <input type="file" class="form-control-file d-inline" id="input-file" name="picture" value="">

                                        </div>
                                    </td>
                                    <td class="product-details">
                                        <ul>
                                            @foreach($member as $key => $value)
                                                <li>
                                                    <span class="status active"><strong>{{$key}}</strong>{{$value}}</span>
                                                </li>

                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="action" data-title="Action">
                                        <div class="">
                                            <ul class="list-inline justify-content-center">
                                                <li class="list-inline-item">
                                                    <a data-toggle="tooltip" data-placement="top"
                                                       title="Tooltip on top"
                                                       class="view" href="">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="edit" href="">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="delete" href="">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{$unfilter->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </section>

@endsection


@section('javascript')

@endsection