@extends('layout.app')



@section('content')


    <section class="dashboard section">
        <!-- Container Start -->
        <div class="container">
            <!-- Row Start -->
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
                    <div class="sidebar">
                        <!-- User Widget -->

                        <!-- Dashboard Links -->
                        <div class="widget user-dashboard-menu">
                            <ul>
                                <li><a href="{{route('clients')}}"><i class="fa fa-user"></i> Nos clients
                                        <span>{{\App\Member::all()->count()}}</span></a></li>
                                <li class="active"><a href="{{route('userList')}}"><i class="fa fa-user"></i> Nos
                                        partenaires <span>{{0}}</span></a></li>
                                <!-- <li><a href=""><i class="fa fa-bookmark-o"></i> Favourite Ads <span>5</span></a></li>
                                 <li><a href=""><i class="fa fa-file-archive-o"></i>Archived Ads <span>12</span></a></li>
                                 <li><a href=""><i class="fa fa-bolt"></i> Pending Approval<span>23</span></a></li>
                                 <li><a href=""><i class="fa fa-cog"></i> Logout</a></li>
                                 <li><a href=""><i class="fa fa-power-off"></i>Delete Account</a></li>-->
                            </ul>

                        </div>
                        <div>
                            <a class=" add-button" href="{{route('addUser')}}"><i class="fa fa-plus-circle"></i> Ajouter
                                Partenaires</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                    <!-- Edit Personal Info -->
                    <div class="widget personal-info">
                        <h3 class="widget-header user">Modifier Son Profil</h3>

                        <form action="{{route('update')}}" method="POST">
                        {{csrf_field()}}

                        <!-- First Name -->
                            <div class="form-group">
                                <label for="firstName">Nom</label>
                                <input type="text" name="firstName" class="form-control" id="firstName"  required
                                       value="{{$user->firstName or old('firstName')}}">

                                <div>
                                    @if ($errors->has('firstName'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Last Name -->
                            <div class="form-group">
                                <label for="lastName">Prenom</label>
                                <input type="text" name="lastName" class="form-control" id="lastName"  required
                                       value="{{$user->lastName or old('lastName')}}">

                                <div>
                                    @if ($errors->has('lastName'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Phone -->
                            <div class="form-group">
                                <label for="phone">Téléphone</label>
                                <input type="text" name="phone" class="form-control" id="phone"  required
                                       value="{{$user->phone or old('phone')}}">

                                <div>
                                    @if ($errors->has('phone'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <!-- Email -->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text"  name="email" class="form-control" id="email"  required
                                       value="{{$user->email or old('email')}}">

                                <div>
                                    @if ($errors->has('email'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- role -->
                                <div class="form-group">
                                    <label for="role_id">Role </label>
                                    <select name="role_id" class="form-control" required>
                                        @foreach($role as $rol)
                                            <option value="{{$rol->id}}"  @if($rol->id == $user->role_id)
                                            selected
                                                    @endif>{{$rol->name}}</option>

                                        @endforeach
                                    </select>

                                    <div>
                                        @if ($errors->has('role_id'))
                                            <span class="data-error red-text">
                                        <strong>{{ $errors->first('role_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                        <!-- companies -->
                                <div class="form-group">
                                    <label>Companies</label>
                                    <select name="company_id" class="form-control" required>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}" @if($company->id == $user->company_id)
                                            selected
                                                    @endif>{{$company->name}}</option>
                                        @endforeach
                                    </select>

                                    <div>
                                        @if ($errors->has('company_id'))
                                            <span class="data-error red-text">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                        <!-- Submit button -->
                            <button class="btn btn-transparent">Enregistrer</button>
                        </form>

                        </div>
                    <!-- Change Password -->
                    <div class="widget change-password">
                        <h3 class="widget-header user">Changer Mot De Passe</h3>
                        @if (Session::has('success'))
                            <div class="alert alert-success">{!! Session::get('success') !!}</div>
                        @endif
                        @if (Session::has('failure'))
                            <div class="alert alert-danger">{!! Session::get('failure') !!}</div>
                        @endif
                        <form action="{{route('updatec')}}" method="post">
                        {{ csrf_field() }}
                        <!-- Current Password -->
                            <div class="form-group p{{ $errors->has('old') ? ' has-error' : '' }}">
                                <label for="old"> Ancien Mot De Passe</label>
                                <input type="password" name="old" class="form-control" id="old" required>
                            </div>
                            <!-- New Password -->
                            <div class="form-group p{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="new-password"> Nouveau Mot de Passe</label>
                                <input type="password" name="password" class="form-control" id="new-password" required>
                                <div>
                                    @if ($errors->has('password'))
                                        <span class="data-error text-danger red-text small ">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Confirm New Password -->
                            <div class="form-group p{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="confirm-password">Confirmer Le Nouveau Mot De Passe</label>
                                <input type="password" name="password_confirmation" class="form-control" id="confirm-password" required>
                                <div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="data-error text-danger red-text small ">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Submit Button -->
                            <button class="btn btn-transparent">Changer Mot De Passe</button>
                        </form>
                    </div>

                </div>

            </div>
        </div>
                </div>
            </div>
        <!-- Row End -->
        </div>
        <!-- Container End -->
    </section>

@endsection


@section('javascript')

@endsection