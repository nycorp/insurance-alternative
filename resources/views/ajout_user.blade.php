
@extends('layout.app')

@section('titre')
    Profil membre
@endsection

@section('content')


    <!--==================================
    =            User Profile            =
    ===================================-->

    <section class="user-profile section">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
                    <div class="sidebar">
                        <!-- User Widget -->
                        <div class="widget user-dashboard-profile">
                            <!-- User Image -->
                            <div class="profile-thumb">
                                <img src="{{asset('assets/images/user/user-thumb.png')}}" alt="" class="rounded-circle">
                            </div>
                            <!-- User Name -->
                            <h5 class="text-center">{{ Auth::user()->lastName }}</h5>
                            <p>{{\App\Role::where('id',Auth::user()->role_id)->first()->name}}</p>
                            <a href="user-profile.html" class="btn btn-main-sm">Edit Profile</a>
                        </div>
                        <!-- Dashboard Links -->
                        <div class="widget user-dashboard-menu">
                            <ul>
                                <li><a href=""><i class="fa fa-user"></i> Nos Clients
                                        <span>{{\App\Member::all()->count()}}</span></a></li>
                                <li ><a href="{{route('addUser')}}"><i class="fa fa-user"></i> Nos Partenaires <span>4</span></a></li>
                                <!--<li><a href=""><i class="fa fa-cog"></i> Logout</a></li>
                                <li><a href=""><i class="fa fa-power-off"></i>Delete Account</a></li>-->
                               </ul>
                        </div>
                        <div >
                            <a class=" add-button" href="{{route('addUser')}}"><i class="fa fa-plus-circle"></i> Ajouter Partenaires</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
                    <!-- Edit Personal Info -->
                    <div class="widget personal-info">
                        <h3 class="widget-header user">Ajout D'un Nouveau Partenaire</h3>
                        <form action="{{route('register')}}" method="post">
                        {{ csrf_field() }}
                            <!-- First Name -->
                            <div class="form-group">
                                <label for="firstName">Nom</label>
                                <input type="text" name="firstname" class="form-control" id="firstName"  required
                                       value="{{$firstname or old('firstname')}}">

                                <div>
                                    @if ($errors->has('firstname'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Last Name -->
                            <div class="form-group">
                                <label for="lastname">Prenom</label>
                                <input type="text" name="lastname" class="form-control" id="lastName"  required
                                       value="{{$lastName or old('lastname')}}">

                                <div>
                                    @if ($errors->has('lastname'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Phone -->
                            <div class="form-group">
                                <label for="phone">Téléphone</label>
                                <input type="text" name="phone" class="form-control" id="phone"  required
                                       value="{{$phone or old('phone')}}">

                                <div>
                                    @if ($errors->has('phone'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <!-- Email -->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text"  name="email" class="form-control" id="email"  required
                                       value="{{$email or old('email')}}">

                                <div>
                                    @if ($errors->has('email'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- role -->
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select name="role" class="form-control" required>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>

                                <div>
                                    @if ($errors->has('role'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- companies -->
                            <div class="form-group">
                                <label>Companies</label>
                                <select name="company" class="form-control" required>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>

                                <div>
                                    @if ($errors->has('company'))
                                        <span class="data-error red-text">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Submit button -->
                            <button class="btn btn-transparent">Enregistrer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection


@section('javascript')

@endsection
