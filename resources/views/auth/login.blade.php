@extends('layout.app')


@section('content')

    <section class="hero-area bg-1  overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block text-center">
                        <h1>Insurance</h1>
                        <p>Rejoignez les millions de partenaires <br> qui chaque jours travaille avec nous</p>
                        <!--<div class="short-popular-category-list text-center">
                            <h2>Categorie de partenaire</h2>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-bed"></i> Hotel</a></li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-grav"></i> Fitness</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-car"></i> Cars</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-cutlery"></i> Restaurants</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-coffee"></i> Cafe</a>
                                </li>
                            </ul>
                        </div> -->

                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search col-md-6 col-md-offset-3" style="margin-left: 25%">
                        <h2 class="text-center" style="margin-bottom: 5%">Identifiez vous </h2>
                        <form method="post" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email"
                                       value="{{ old('email') }}" required autofocus>
                                <div>
                                    @if ($errors->has('email'))
                                        <span class="help-block label-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password">Mot de passe</label>
                                <input type="password" class="form-control" name="password" id="password" required>
                                <div>
                                    @if ($errors->has('password'))
                                        <span class="help-block label-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-main col-md-12"> Démarrer</button>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Mot de passe oublié?
                                </a>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>

    @component('component.partner')
    @endcomponent
    @component('component.footer')
    @endcomponent
@endsection
@section('footer')
    @endsection
