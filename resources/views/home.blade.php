@extends('layout.app')
@section('content')
    <!--===============================
=            Hero Area            =
================================-->

    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block">
                        <h1>Insurance</h1>
                        <p>Rejoignez les millions de partenaires <br> qui chaque jours travaille avec nous</p>
                        <!--<div class="short-popular-category-list text-center">
                            <h2>Categorie de partenaire</h2>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-bed"></i> Hotel</a></li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-grav"></i> Fitness</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-car"></i> Cars</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-cutlery"></i> Restaurants</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-coffee"></i> Cafe</a>
                                </li>
                            </ul>
                        </div> -->

                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <form action="{{ route('profile') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <!-- Store Search -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="block d-flex">
                                        <input type="text" name="code" class="form-control mb-2 mr-sm-2 mb-sm-0"
                                               id="search"
                                               placeholder="Entrer le code a rechercher" required>
                                        <!-- Search Button -->
                                        <button class="btn btn-main">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>
    @component('component.partner')
    @endcomponent
@endsection


@section('javascript')


@endsection
