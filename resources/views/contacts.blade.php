@extends('layout.app')

@section('content')



    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block">
                        <h1>Insurance</h1>
                        <p>Rejoignez les millions de partenaires <br> qui chaque jours travaille avec nous</p>
                        <!--<div class="short-popular-category-list text-center">
                            <h2>Categorie de partenaire</h2>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-bed"></i> Hotel</a></li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-grav"></i> Fitness</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-car"></i> Cars</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-cutlery"></i> Restaurants</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href=""><i class="fa fa-coffee"></i> Cafe</a>
                                </li>
                            </ul>
                        </div> -->

                    </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>

        </section>
    <section class=" section">
        <div class="container">
            <div class="row">
                    <div class="" id="contact1">
                        <div class="row">
                            <div class="col-sm-4">
                                <h3><i class="fa fa-map-marker"></i> Address</h3>
                                <p> CAMEROUN / USA<br/><br/>

                                </p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-phone"></i> Centre d'appel</h3>
                                <p class="text-muted"></p>
                                <p>(+1)424 356 2592<br>
                                    (+237)6 55 04 70 50 <br></p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-envelope"></i>Support électronique</h3>
                                <p class="text-muted">N'hésitez pas à nous écrire un mail</p>
                                <ul>
                                    <li><strong><a href="mailto:">www.letsrealize.com</a></strong>   </li>
                                </ul>
                            </div>
                            <!-- /.col-sm-4 -->
                        </div>
                        <!-- /.row -->
                        <hr>

                        <hr>
                        <h2>Remplisez ce formulaire pour pouvoir nous contacter</h2>
                        <form action="" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="firstName">Nom</label>
                                        <input type="text" name="firstname" class="form-control" id="firstName" required
                                               value="{{$firstName or old('firstname')}}">

                                        <div>
                                            @if ($errors->has('firstname'))
                                                <span class="data-error red-text">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lastName">Prénom</label>
                                        <input type="text" name="lastname" class="form-control" id="lastName" required
                                               value="{{$lastName or old('lastname')}}">

                                        <div>
                                            @if ($errors->has('lastname'))
                                                <span class="data-error red-text">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text"  name="email" class="form-control" id="email" required
                                               value="{{$email or old('email')}}">

                                        <div>
                                            @if ($errors->has('email'))
                                                <span class="data-error red-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="subject">Sujet</label>
                                        <input type="text" name="subject" class="form-control" id="subject" required
                                               value="{{old('subject')}}">

                                        <div>
                                            @if ($errors->has('subject'))
                                                <span class="data-error red-text">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea  id="message" name="message" class="textearea"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-12  text-center">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Envoyer le message</button>
                                </div>
                            </div>
                            <!-- /.row -->
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </section>
    @component('component.partner')
    @endcomponent
@endsection