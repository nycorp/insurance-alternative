@extends('layout.app')

@section('titre')
    Profil membre
@endsection

@section('content')
    <section class="page-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <form action="{{ route('profile') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <!-- Store Search -->
                                <div class="col-lg-12 col-md-12">
                                    <div class="block d-flex">
                                        <input type="text" name="code" class="form-control mb-2 mr-sm-2 mb-sm-0"
                                               id="search"
                                               placeholder="Entrer le code a rechercher" required>
                                        <!-- Search Button -->
                                        <button class="btn btn-success">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard section">
        <!-- Container Start -->
        <div class="container">
            <!-- Row Start -->
            @if($found)
                <div class="row">
                    <div class="col-md-2 offset-md-0 col-lg-12 offset-lg-0">
                        <!-- Recently Favorited -->
                        <div class="widget dashboard-container my-adslist">
                            <h3 class="widget-header">Informations personelles</h3>
                            <table class="table table-responsive product-dashboard-table">
                                <tbody>
                                <tr>
                                    <td class="product-thumb">
                                        <div class="profile-thumb">
                                            <img src="{{asset('assets/images/user/user-thumb.png')}}"
                                                 alt="Member picture" class="rounded-circle">
                                        </div>
                                    </td>
                                    <td class="product-category"><span class="categories"></span></td>
                                    <td class="product-details">
                                        @foreach($member as $key => $value)
                                            <span class="status active"><strong>{{$key}}</strong>{{$value}}</span>
                                            {{--<span><strong>Posted on: </strong><time>Jun 27, 2017</time> </span>
                                            <span class="status active"><strong>Status</strong>Active</span>
                                            <span class="location"><strong>Location</strong>Dhaka,Bangladesh</span>--}}
                                        @endforeach
                                    </td>
                                    <td class="product-category"><span class="categories"></span></td>
                                    <td class="action" data-title="Action">
                                        <div class="">
                                            <ul class="list-inline justify-content-center">
                                                <li class="list-inline-item">
                                                    <a data-toggle="tooltip" data-placement="top" title="Tooltip on top"
                                                       class="view" href="">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="edit" href="">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="delete" href="">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- Row End -->
            @else
                <div class="widget rate">
                    <!-- Heading -->
                    <h3 class="widget-header text-center"><i class="fa fa-warning"></i>
                        <br>Désoler nous n'avons rien
                        trouvé
                        <br>
                        sur cette personne</h3>
                    <!-- Rate -->
                    <div class="starrr"></div>
                </div>

            @endif
        </div>
        <!-- Container End -->
    </section>
@endsection


@section('javascript')
@endsection