<footer class="footer section section-sm">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">
                <!-- About -->
                <div class="block about">
                    <!-- footer logo -->
                    <img src="{{asset('assets/images/logo-footer.png')}}" alt="">
                    <!-- description -->
                    <p class="alt-color">
                    <p>Rejoignez les millions de partenaires qui chaque jours travaille avec nous
                    </p>
                </div>
            </div>
            <!-- Link list -->
            <div class="col-lg-4 offset-lg-1 col-md-3">
                <div class="block">
                    <h4>Pages du site</h4>
                    <ul>
                        <li><a href="{{route('contact')}}">Contactez-nous</a></li>
                    </ul>
                </div>
            </div>
            <!-- Link list -->

            <!-- Promotion -->
            <div class="col-lg-4 col-md-7">
                <!-- App promotion -->
                <div class="block-2 app-promotion">
                    <a href="">
                        <!-- Icon -->
                        <img src="{{asset('assets/images/footer/phone-icon.png')}}" alt="mobile-icon">
                        <i class="fa fa-android icon-bg-2"></i>
                    </a>
                    <p>Télécharger notre application <br> sur Play Store</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Container End -->
</footer>
<!-- Footer Bottom -->
<footer class="footer-bottom">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-12">
                <!-- Copyright -->
                <div class="copyright">
                    <p>Copyright © 2018. All Rights Reserved</p>
                </div>
            </div>
            <div class="col-sm-6 col-12">
                <!-- Social Icons -->
                <ul class="social-media-icons text-right">
                    <li><a class="fa fa-facebook" href="#"></a></li>
                    <li><a class="fa fa-twitter" href="#"></a></li>
                    <li><a class="fa fa-pinterest-p" href="#"></a></li>
                    <li><a class="fa fa-vimeo" href="#"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Container End -->
    <!-- To Top -->
    <div class="top-to">
        <a id="top" class="" href="#"><i class="fa fa-angle-up"></i></a>
    </div>
</footer>