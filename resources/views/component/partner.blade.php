<!--===================================
=            Client Slider            =
====================================-->


<section class=" section">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section title -->
                <div class="section-title">
                    <h2>Partenaires actuels</h2>
                    <p>Quelques services en partenariat avec nous!!!</p>
                </div>
                <div class="row">
                    <!-- Category list -->
                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                        <div class="category-block">
                            <div class="header">
                                <i class="fa fa-hospital-o icon-bg-1"></i>
                                <h4>Santé</h4>
                            </div>
                            <ul class="category-list">
                                <li><a href="#">Hopital <span>10</span></a></li>
                                <li><a href="#">Dentiste <span>233</span></a></li>
                                <li><a href="#">Pharmacie <span>183</span></a></li>
                                <li><a href="#">Pédiatrie <span>343</span></a></li>
                            </ul>
                        </div>
                    </div> <!-- /Category List -->
                    <!-- Category list -->
                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                        <div class="category-block">
                            <div class="header">
                                <i class="fa fa-briefcase icon-bg-5"></i>
                                <h4>Travails</h4>
                            </div>
                            <ul class="category-list">
                                <li><a href="#">It Jobs <span>93</span></a></li>
                                <li><a href="#">Cleaning & Washing <span>233</span></a></li>
                                <li><a href="#">Management <span>183</span></a></li>
                                <li><a href="#">Voluntary Works <span>343</span></a></li>
                            </ul>
                        </div>
                    </div> <!-- /Category List -->
                    <!-- Category list -->
                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                        <div class="category-block">
                            <div class="header">
                                <i class="fa fa-car icon-bg-6"></i>
                                <h4>Vehicules</h4>
                            </div>
                            <ul class="category-list">
                                <li><a href="#">Bus <span>193</span></a></li>
                                <li><a href="#">Cars <span>23</span></a></li>
                                <li><a href="#">Motobike <span>33</span></a></li>
                                <li><a href="#">Rent a car <span>73</span></a></li>
                            </ul>
                        </div>
                    </div> <!-- /Category List -->
                    <!-- Category list -->
                    <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                        <div class="category-block">
                            <div class="header">
                                <i class="fa fa-apple icon-bg-2"></i>
                                <h4>Restaurants</h4>
                            </div>
                            <ul class="category-list">
                                <li><a href="#">Cafe <span>393</span></a></li>
                                <li><a href="#">Fast food <span>23</span></a></li>
                                <li><a href="#">Restaurants <span>13</span></a></li>
                                <li><a href="#">Food Track<span>43</span></a></li>
                            </ul>
                        </div>
                    </div> <!-- /Category List -->


                </div>
            </div>
        </div>
    </div>
    <!-- Container End -->
</section>