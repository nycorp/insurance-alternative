<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('main', function () {
    return view('main_view');
})->name('main_view');
Route::get('contact', function () {
    return view('contacts');
})->name('contact');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('contact', 'Web\UserController@contactUs')->name('contact');

Route::get('profile', 'Web\MemberController@find')->name('profile');
Route::Post('profile', 'Web\MemberController@find')->name('profile');
Route::get('clients', 'Web\MemberController@getAllMember')->name('clients');
Route::get('user/add', 'Web\UserController@getAddUserForm')->name('addUser');
Route::get('user/list', 'Web\UserController@getAllUser')->name('userList');
Route::get('/edit', 'Web\UserController@edit')->name('edit');
Route::Post('/update', 'Web\UserController@modifier')->name('update');
Route::get('/profil', 'Web\UserController@change')->name('updatep');
Route::post('/profil', 'Web\UserController@update')->name('updatec');
Route::post('update/members', 'Web\MemberController@updateMembersList')->name('update/members');