<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('mobile/auth/user/login', 'Mobile\Auth\User\LoginController@login');

Route::group(['BaseMiddleware' => 'jwt.auth'], function () {
    Route::post('mobile/member/find', 'Mobile\User\MemberController@find');
    Route::post('mobile/member/add/note', 'Mobile\User\MemberController@postNote');
});
