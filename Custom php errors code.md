############################################################################
#                                                                          #
#                                                                          #
#             #######       #######      #########       #########         #
#           ##       #    ##       ##    ##       #      ##                #
#           ##            ##       ##    ##       ##     ##                #
#           ##            ##       ##    ##       ##     ##                #
#           ##            ##       ##    ##       ###    ##                #
#           ##            ##       ##    ##       ###    #######           #
#           ##            ##       ##    ##       ###    ##                #
#           ##            ##       ##    ##       ##     ##                #
#           ##            ##       ##    ##       ##     ##                #
#           ##       #    ##       ##    ##       #      ##                #
#             #######       #######      #########       #########         #
#                                                                          #
#                                                                          #
############################################################################

Code ==>> Signification

***************Authentication***************

1000 ==>> LOGIN SUCCESSFULLY
1001 ==>> WRONG USERNAME
1002 ==>> WRONG PASSWORD
1003 ==>> WRONG USERNAME OR PASSWORD
1004 ==>> MISSING REQUIRE CREDENTIALS INFORMATION

*****************Registration*****************

1100 ==>> ACCOUNT CREATE SUCCESSFULLY
1101 ==>> ACCOUNT ALREADY EXIST
1102 ==>> INVALID EMAIL ADDRESS
1103 ==>> UNABLE TO SAVE TO DATABASE

*****************Verification*****************

1200 ==>> ACCOUNT ACTIVATED SUCCESSFULLY
1201 ==>> ACCOUNT IS NOT ACTIVATE
1202 ==>> ACCOUNT CONFIRMATION CODE EXPIRED OR DELETED
1203 ==>> ACCOUNT CONFIRMATION MISSING INFORMATION
1204 ==>> ACCOUNT CONFIRMATION CODE SEND SUCCESSFULLY
1205 ==>> ACCOUNT ALREADY ACTIVE
1206 ==>> ACCOUNT CONFIRMATION CODE SEND FAILURE

*****************File*****************

1300 ==>> BAD FILE EXTENSION

*****************Member*****************

1400 ==>> MISSING REQUIRED INFORMATION
1401 ==>> MEMBER_NOT_FOUND
1402 ==>> MEMBER_FOUND

*****************Token*****************

1 ==>> TOKEN EXPIRED
2 ==>> INVALID TOKEN
3 ==>> NO TOKEN
4 ==>> USER NOT FOUND
5 ==>> NO_DEVICE_ID_FOUND

*****************Note*****************

1500 ==>> NOTE_POST_SUCCESSFULLY
1501 ==>> NOTE_POST_FAILURE
1502 ==>> NOTE_POST_MISSING_INFORMATION

